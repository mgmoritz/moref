(defcustom
  moref--browser
  'eww
  "Browser to search for language reference"
  :type 'symbol
  :group 'moref)

(defcustom moref--lang-url-alist
  '((lisp-interaction-mode . "gnu.org")
    (emacs-lisp-mode . "gnu.org")
    (go-mode . "golang.org")
    (java-mode . "processing.org")
    (clojure-mode . "clojuredocs.org"))
  "An alist specifying mappings of documentation search urls
To be used directly in the code to browse in specific language
documentation."
  :type '(alist :key-type symbol :value-type string)
  :group 'moref)

(defun moref-add-reference-to-language ()
  (interactive)
  (customize-save-variable
   'moref--lang-url-alist
   (add-to-list 'moref--lang-url-alist
                `(,major-mode . ,(read-string "Language documentation website: "
                                              nil
                                              nil
                                              "lang.org")))))

(defun moref-browse-reference ()
  "Search gnu.org using duckduckgo for word in region or ask for query"
  (interactive)
  (let ((search (if (use-region-p)
                    (buffer-substring-no-properties (region-beginning) (region-end))
                  (read-string "search in processing: ")))
        (reference (alist-get major-mode moref--lang-url-alist)))
    (if reference
        (apply moref--browser `(,(format "https://duckduckgo.com/?q=\\%s site:%s&t=h_" search reference)))
      (apply moref--browser `(,(format "https://duckduckgo.com/?q=%s&t=h_" search))))))

(provide 'moref)
